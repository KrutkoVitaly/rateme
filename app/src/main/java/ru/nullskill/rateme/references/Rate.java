package ru.nullskill.rateme.references;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Rate {

    public String comment;
    private long date;
    public String quality;
    public int points;
    public String sender;
    private String receiver;

    public Rate() {
    }

    public Rate(String comment, long date, String quality, int points, String sender, String receiver) {
        this.comment = comment;
        this.date = date;
        this.quality = quality;
        this.points = points;
        this.sender = sender;
        this.receiver = receiver;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("comment", comment);
        result.put("date", date);
        result.put("quality", quality);
        result.put("points", points);
        result.put("sender", sender);
        result.put("receiver", receiver);
        return result;
    }
}