package ru.nullskill.rateme.references;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Dialog {

    public String id;
    public String name;
    public String lastMessage;

    public Dialog() {
    }

    public Dialog(String id, String name, String lastMessage) {
        this.id = id;
        this.name = name;
        this.lastMessage = lastMessage;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("name", name);
        result.put("lastMessage", lastMessage);

        return result;
    }
}
