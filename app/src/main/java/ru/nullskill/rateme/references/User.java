package ru.nullskill.rateme.references;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class User {

    public String id;
    private String email;
    public String name;
    private String surname;
    public String city;
    public String country;
    public int rates;
    public int views;

    public User() {
    }

    public User(String id, String email, String name, String surname, String city, String country, int rates, int views) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.country = country;
        this.rates = rates;
        this.views = views;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("email", email);
        result.put("name", name);
        result.put("surname", surname);
        result.put("country", country);
        result.put("city", city);
        result.put("rates", rates);
        result.put("views", views);

        return result;
    }
}