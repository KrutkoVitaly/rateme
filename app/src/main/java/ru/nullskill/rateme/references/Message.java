package ru.nullskill.rateme.references;

import java.util.Date;

public class Message {

    private String messageText;
    private String sender;
    private long messageTime;
    private String senderStatus;
    private String receiverStatus;

    public Message() {
    }

    public Message(String messageText, String sender, String senderStatus, String receiverStatus) {
        this.messageText = messageText;
        this.sender = sender;
        this.messageTime = new Date().getTime();
        this.senderStatus = senderStatus;
        this.receiverStatus = receiverStatus;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }

    public String getSenderStatus() {
        return senderStatus;
    }

    public void setSenderStatus(String senderStatus) {
        this.senderStatus = senderStatus;
    }

    public String getReceiverStatus() {
        return receiverStatus;
    }

    public void setReceiverStatus(String receiverStatus) {
        this.receiverStatus = receiverStatus;
    }
}
