package ru.nullskill.rateme.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ru.nullskill.rateme.AuthActivity;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.util.Storage;

public class CheckNotifications extends Service {

    public CheckNotifications() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Storage.init(CheckNotifications.this);
        DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference().child("notifications").child(Storage.getString("UID", ""));
        notificationReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null && Storage.getBoolean("enable_notification", true)) {
                    Intent intent = new Intent(CheckNotifications.this, AuthActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(CheckNotifications.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(CheckNotifications.this)
                            .setSmallIcon(R.mipmap.notification)
                            .setLargeIcon(bitmap)
                            .setContentTitle("RateMe")
                            .setContentText(String.valueOf(dataSnapshot.getValue()))
                            .setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_SOUND)
                            .setSound(defaultSoundUri)
                            .setVibrate(new long[]{0, 300, 0, 300, 0, 300})
                            .setContentIntent(pendingIntent);

                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (notificationManager != null) {
                        notificationManager.notify(0, notificationBuilder.build());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return super.onStartCommand(intent, flags, startId);
    }
}
