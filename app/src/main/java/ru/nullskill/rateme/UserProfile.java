package ru.nullskill.rateme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.ads.MobileAds;

import ru.nullskill.rateme.adapters.UserPager;
import ru.nullskill.rateme.util.Storage;

public class UserProfile extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        App.context = getApplicationContext();
        App.window = getWindow();
        App.userActivity = UserProfile.this;

        Storage.init(getApplicationContext());
        MobileAds.initialize(this, "ca-app-pub-4982253629578691~4150446278");

        initNavigation();
        initViewPager();
    }

    private void initNavigation() {
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @SuppressLint("InflateParams")
    private void initViewPager() {
        viewPager = findViewById(R.id.view_pager);
        UserPager userPager = new UserPager(getSupportFragmentManager());
        viewPager.setAdapter(userPager);
        viewPager.setCurrentItem(getIntent().getIntExtra("fragment", 1));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                App.hideButtons(App.window);
            }

            @Override
            public void onPageSelected(int position) {
                navigation.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dialog:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_user:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_qualities:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        App.hideNavigation(getWindow());
    }

}