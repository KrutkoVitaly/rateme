package ru.nullskill.rateme;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.references.Rate;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.ratesReference;
import static ru.nullskill.rateme.App.skillsReference;
import static ru.nullskill.rateme.App.thumbsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class History extends AppCompatActivity {

    private LinearLayout historyContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Storage.init(getApplicationContext());

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        historyContainer = findViewById(R.id.history_container);
        Query getMyRates = ratesReference.child(Storage.getString("UID", "")).limitToLast(50);
        getMyRates.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                historyContainer.removeAllViews();
                ArrayList<View> viewSet = new ArrayList<>();
                viewSet.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final Rate rate = postSnapshot.getValue(Rate.class);
                    if (rate != null) {
                        View rateCard = getLayoutInflater().inflate(R.layout.last_rate_card, null);
                        final CircleImageView rateAvatar = rateCard.findViewById(R.id.rate_avatar);
                        final TextView name = rateCard.findViewById(R.id.name);
                        final TextView comment = rateCard.findViewById(R.id.comment);
                        final TextView quality = rateCard.findViewById(R.id.quality);

                        if (rate.quality != null) {
                            Query getSkillsRates = skillsReference.child(rate.quality);
                            getSkillsRates.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String text = "";
                                    if (rate.points == -1) {
                                        quality.setBackgroundResource(R.drawable.black_round_corners);
                                        if (Locale.getDefault().getLanguage().equals("ru")) {
                                            text = String.valueOf("- " + dataSnapshot.child("title_ru").getValue());
                                        } else {
                                            text = String.valueOf("- " + dataSnapshot.getKey().substring(0, 1).toUpperCase()+dataSnapshot.getKey().substring(1,dataSnapshot.getKey().length()));
                                        }
                                    }
                                    if (rate.points == 1) {
                                        quality.setBackgroundResource(R.drawable.round_corners);
                                        StateListDrawable shapeDrawable = (StateListDrawable) quality.getBackground();
                                        shapeDrawable.setColorFilter(Color.parseColor(String.valueOf(dataSnapshot.child("line_color").getValue())), PorterDuff.Mode.MULTIPLY);
                                        if (Locale.getDefault().getLanguage().equals("ru")) {
                                            text = String.valueOf("+ " + dataSnapshot.child("title_ru").getValue());
                                        } else {
                                            text = String.valueOf("+ " + dataSnapshot.getKey().substring(0, 1).toUpperCase()+dataSnapshot.getKey().substring(1,dataSnapshot.getKey().length()));
                                        }
                                    }
                                    quality.setText(text);
                                    quality.setTextColor(Color.WHITE);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }

                        if (String.valueOf(rate.comment).length() == 0) {
                            comment.setVisibility(View.GONE);
                        } else {
                            comment.setText(String.valueOf(rate.comment));
                        }
                        rateAvatar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(App.context, UserProfile.class)
                                        .putExtra("fragment", 1)
                                        .putExtra("uid", rate.sender));
                            }
                        });
                        rateAvatar.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                final ImageView photoDialog = new ImageView(getApplicationContext());
                                photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                photoDialog.setImageDrawable(rateAvatar.getDrawable());

                                avatarsStorage.child(rate.sender + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Glide.with(getApplicationContext())
                                                .load(uri)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .into(photoDialog);

                                        new AlertDialog.Builder(History.this)
                                                .setView(photoDialog)
                                                .show();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {

                                    }
                                });

                                return true;
                            }
                        });
                        usersReference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                name.setText(String.valueOf(dataSnapshot.child(rate.sender).child("name").getValue()));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        thumbsStorage.child(rate.sender + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Glide.with(getApplicationContext())
                                        .load(uri)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(rateAvatar);
                            }
                        });
                        viewSet.add(rateCard);
                    }
                }
                for (int i = viewSet.size() - 1; i >= 0; i--) {
                    historyContainer.addView(viewSet.get(i));
                    if (i % 10 == 0) {
                        final View adContainer = getLayoutInflater().inflate(R.layout.ad_card, null);
                        LinearLayout adContainerLayout = adContainer.findViewById(R.id.ad_container);

                        AdView mAdView = new AdView(History.this);
                        mAdView.setAdUnitId(getString(R.string.adUnitId));
                        mAdView.setAdSize(AdSize.FLUID);
                        AdRequest adRequest = new AdRequest.Builder().build();
                        mAdView.loadAd(adRequest);
                        adContainerLayout.addView(mAdView);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        App.hideNavigation(getWindow());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}