package ru.nullskill.rateme.model;

import android.net.Uri;

public class FeedItem {

    private int id;
    private Uri photo;
    private Object likes;
    private Object dislikes;

    public FeedItem() {
    }

    public FeedItem(int id, Uri photo, Object likes, Object dislikes) {
        this.id = id;
        this.photo = photo;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public Object getLikes() {
        return likes;
    }

    public void setLikes(Object likes) {
        this.likes = likes;
    }

    public Object getDislikes() {
        return dislikes;
    }

    public void setDislikes(Object dislikes) {
        this.dislikes = dislikes;
    }
}