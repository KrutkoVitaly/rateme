package ru.nullskill.rateme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.ads.MobileAds;

import ru.nullskill.rateme.adapters.Pager;
import ru.nullskill.rateme.services.CheckNotifications;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.isFirstLaunch;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Storage.init(getApplicationContext());

        App.context = getApplicationContext();
        App.window = getWindow();
        App.activity = MainActivity.this;

        DisplayMetrics displaymetrics = getResources().getDisplayMetrics();
        Storage.addInt("display_width", displaymetrics.widthPixels);

        MobileAds.initialize(this, "ca-app-pub-4982253629578691~4150446278");

        startService(new Intent(MainActivity.this, CheckNotifications.class));

        initNavigation();
        initViewPager();
        isFirstLaunch(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.hideNavigation(getWindow());
    }

    private void initNavigation() {
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @SuppressLint("InflateParams")
    private void initViewPager() {
        viewPager = findViewById(R.id.view_pager);
        Pager pager = new Pager(getSupportFragmentManager());
        viewPager.setAdapter(pager);
        viewPager.setCurrentItem(2);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                App.hideNavigation(getWindow());
                hideKeyboard();
            }

            @Override
            public void onPageSelected(int position) {
                navigation.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_search:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_chart:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_home:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_history:
                    viewPager.setCurrentItem(3);
                    return true;
                case R.id.navigation_people:
                    viewPager.setCurrentItem(4);
                    return true;
            }
            return false;
        }
    };

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}