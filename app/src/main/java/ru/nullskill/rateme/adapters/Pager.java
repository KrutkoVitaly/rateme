package ru.nullskill.rateme.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.nullskill.rateme.fragments.Feed;
import ru.nullskill.rateme.fragments.Profile;
import ru.nullskill.rateme.fragments.Qualities;
import ru.nullskill.rateme.fragments.Search;
import ru.nullskill.rateme.fragments.Settings;

public class Pager extends FragmentStatePagerAdapter {

    public Pager(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Search();
            case 1:
                return new Qualities();
            case 2:
                return new Profile();
            case 3:
                return new Feed();
            case 4:
                return new Settings();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
