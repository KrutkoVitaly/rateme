package ru.nullskill.rateme.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.nullskill.rateme.fragments.user.Qualities;
import ru.nullskill.rateme.fragments.user.Dialog;
import ru.nullskill.rateme.fragments.user.User;

public class UserPager extends FragmentStatePagerAdapter {

    public UserPager(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Dialog();
            case 1:
                return new User();
            case 2:
                return new Qualities();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
