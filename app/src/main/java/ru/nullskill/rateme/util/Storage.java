package ru.nullskill.rateme.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Storage {

    //Локальное хранилище для данных
    private static SharedPreferences sharedPreferences = null;
    private static SharedPreferences.Editor editor = null;
    private static Context context = null;

    //Передача контекста
    public static void init(Context ctx) {
        context = ctx;
    }

    //Инициализация хранилища пар
    private static void init() {
        String STORAGE_NAME = "RATEME_STORAGE";
        sharedPreferences = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    //Добавление строковых значений через точечный синтаксис в других частях программы
    public static void addString(String key, String value) {
        if (sharedPreferences == null)
            init();
        editor.putString(key, value);
        editor.commit();
    }

    //Получение строковых значений через точечный синтаксис в других частях программы
    public static String getString(String key, String defaultValue) {
        if (sharedPreferences == null)
            init();
        return sharedPreferences.getString(key, defaultValue);
    }

    //Аналогично для других типов данных
    public static void addBoolean(String key, boolean value) {
        if (sharedPreferences == null)
            init();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        if (sharedPreferences == null)
            init();
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public static void addInt(String key, int value) {
        if (sharedPreferences == null)
            init();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(String key, int defaultValue) {
        if (sharedPreferences == null)
            init();
        return sharedPreferences.getInt(key, defaultValue);
    }

    public static void addFloat(String key, float value) {
        if (sharedPreferences == null)
            init();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static float getFloat(String key, float defaultValue) {
        if (sharedPreferences == null)
            init();
        return sharedPreferences.getFloat(key, defaultValue);
    }

    public static void addLong(String key, long value) {
        if (sharedPreferences == null)
            init();
        editor.putLong(key, value);
        editor.commit();
    }

    public static long getLong(String key, long defaultValue) {
        if (sharedPreferences == null)
            init();
        return sharedPreferences.getLong(key, defaultValue);
    }

    //Очистка значения по ключу
    public static void clear(String key) {
        if (sharedPreferences == null)
            init();
        editor.remove(key);
        editor.commit();
    }
}