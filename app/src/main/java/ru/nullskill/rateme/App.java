package ru.nullskill.rateme;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import ru.nullskill.rateme.util.Storage;

public class App {

    public static final DatabaseReference ratesReference = FirebaseDatabase.getInstance().getReference().child("rates");
    public static final DatabaseReference usersReference = FirebaseDatabase.getInstance().getReference().child("users");
    public static final DatabaseReference skillsReference = FirebaseDatabase.getInstance().getReference().child("skills");
    public static final DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference().child("notifications");
    public static final StorageReference avatarsStorage = FirebaseStorage.getInstance().getReference().child("images").child("avatars");
    public static final StorageReference postsStorage = FirebaseStorage.getInstance().getReference().child("images").child("posts");
    public static final StorageReference thumbsStorage = FirebaseStorage.getInstance().getReference().child("images").child("thumbs");
    public static Context context;
    public static Window window;
    public static Activity activity;
    public static Activity userActivity;

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = null;
        if (cm != null) {
            wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        if (cm != null) {
            wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        }
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        if (cm != null) {
            wifiInfo = cm.getActiveNetworkInfo();
        }
        return wifiInfo != null && wifiInfo.isConnected();
    }

    public static void hideNavigation(Window window) {
        View decorView = window.getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static void hideButtons(Window window) {
        View decorView = window.getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static boolean isFirstLaunch(Context context) {
        Storage.init(context);
        return Storage.getBoolean("FIRST_LAUNCH", true);
    }
}