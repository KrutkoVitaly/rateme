package ru.nullskill.rateme;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.references.Dialog;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class Dialogs extends AppCompatActivity {

    private final ArrayList<Dialog> dialogs = new ArrayList<>();
    private LinearLayout usersContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogs);
        Storage.getString("UID", "");

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        usersContainer = findViewById(R.id.users_container);
        usersReference.child(Storage.getString("UID", "")).child("dialogs").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dialogs.clear();
                for (final DataSnapshot dialogSnapshot : dataSnapshot.getChildren()) {
                    usersReference.child(dialogSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot user) {
                            usersReference.child(Storage.getString("UID", "")).child("dialogs").child(user.getKey()).limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (final DataSnapshot message : dataSnapshot.getChildren()) {
                                        dialogs.add(new Dialog(
                                                String.valueOf(dialogSnapshot.getKey()),
                                                String.valueOf(user.child("name").getValue()),
                                                String.valueOf(message.child("messageText").getValue())
                                        ));
                                        filterDialogList("");
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        EditText searchDialogField = findViewById(R.id.search_people_field);
        searchDialogField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                App.hideNavigation(App.window);
            }

            @Override
            public void onTextChanged(CharSequence s, final int start, int before, int count) {
                if (s.length() > 0) {
                    filterDialogList(String.valueOf(s).toLowerCase());
                } else {
                    filterDialogList("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        App.hideNavigation(getWindow());
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    private void filterDialogList(String word) {
        usersContainer.removeAllViews();
        for (int i = dialogs.size() - 1; i >= 0; i--) {

            View view = getLayoutInflater().inflate(R.layout.dialog_row, null);
            final CircleImageView avatar = view.findViewById(R.id.search_photo);

            avatarsStorage.child(dialogs.get(i).id + ".jpg")
                    .getDownloadUrl()
                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(App.context)
                                    .load(uri)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(avatar);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            avatar.setImageResource(R.mipmap.default_user);
                        }
                    });

            TextView name = view.findViewById(R.id.name);
            TextView lastMessage = view.findViewById(R.id.lastMessage);
            name.setText(dialogs.get(i).name);
            lastMessage.setText(dialogs.get(i).lastMessage);

            final int finalI = i;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(App.context, UserProfile.class)
                            .putExtra("fragment", 0)
                            .putExtra("uid", dialogs.get(finalI).id));
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final ImageView photoDialog = new ImageView(getApplicationContext());
                    photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    photoDialog.setImageDrawable(avatar.getDrawable());

                    Glide.with(getApplicationContext())
                            .using(new FirebaseImageLoader())
                            .load(App.avatarsStorage.child(dialogs.get(finalI).id + ".jpg"))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(photoDialog);

                    new AlertDialog.Builder(Dialogs.this)
                            .setView(photoDialog)
                            .show();

                    return true;
                }
            });
            if (dialogs.get(i).name.toLowerCase().contains(word)) {
                usersContainer.addView(view);
            }
        }
    }
}
