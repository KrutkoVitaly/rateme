package ru.nullskill.rateme;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ru.nullskill.rateme.util.Storage;

public class AuthActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText email, password;
    private final String[] myData = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        Storage.init(getApplicationContext());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mAuth = FirebaseAuth.getInstance();

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        if (!App.hasConnection(getApplicationContext())) {
            startActivity(new Intent(AuthActivity.this, NoInternet.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        } else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference().child("notifications").child(user.getUid());
                notificationReference.setValue(null);
                Storage.addString("UID", user.getUid());
                startActivity(new Intent(getApplicationContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        }
    }

    private void createUser(String email, String password) {
        final ProgressDialog progressDialog = new ProgressDialog(AuthActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.creating_user));
        progressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                DatabaseReference usersReference = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid());
                                usersReference.child("email").setValue(user.getEmail());
                                usersReference.child("name").setValue(myData[0] + " " + myData[1]);
                                usersReference.child("country").setValue(myData[2]);
                                usersReference.child("city").setValue(myData[3]);
                                usersReference.child("points").setValue(100);

                                usersReference.child("rates").setValue(0);
                                usersReference.child("views").setValue(0);
                                Storage.addString("UID", user.getUid());
                                startActivity(new Intent(AuthActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            }
                        } else {
                            Toast.makeText(AuthActivity.this, R.string.creating_user_error, Toast.LENGTH_SHORT).show();
                        }
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    private void loginUser(String email, String password) {
        final ProgressDialog progressDialog = new ProgressDialog(AuthActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.signing_in));
        progressDialog.show();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Storage.addString("UID", user.getUid());
                                startActivity(new Intent(AuthActivity.this, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            }
                        } else {
                            Toast.makeText(AuthActivity.this, R.string.signing_in_error,
                                    Toast.LENGTH_SHORT).show();
                        }
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    @SuppressLint("InflateParams")
    public void signUpClick(View view) {
        if (email.getText().length() > 0 && password.getText().length() >= 6) {
            final View myUsernameDialog = getLayoutInflater().inflate(R.layout.my_username_dialog, null);
            final View myLocationDialog = getLayoutInflater().inflate(R.layout.my_location_dialog, null);
            final EditText name = myUsernameDialog.findViewById(R.id.name);
            final EditText surname = myUsernameDialog.findViewById(R.id.surname);
            final EditText country = myLocationDialog.findViewById(R.id.country);
            final EditText city = myLocationDialog.findViewById(R.id.city);
            TextView usernameNext = myUsernameDialog.findViewById(R.id.ok);
            TextView usernameCancel = myUsernameDialog.findViewById(R.id.cancel);
            TextView locationOk = myLocationDialog.findViewById(R.id.ok);
            TextView locationCancel = myLocationDialog.findViewById(R.id.cancel);
            usernameNext.setText(String.valueOf(getString(R.string.next)));

            final AlertDialog usernameDialog = new AlertDialog.Builder(AuthActivity.this)
                    .setView(myUsernameDialog)
                    .setCancelable(false)
                    .show();
            final AlertDialog[] locationDialog = new AlertDialog[1];

            usernameNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (name.getText().length() > 0 && surname.getText().length() > 0) {
                        myData[0] = String.valueOf(name.getText());
                        myData[1] = String.valueOf(surname.getText());
                        locationDialog[0] = new AlertDialog.Builder(AuthActivity.this)
                                .setView(myLocationDialog)
                                .setCancelable(false)
                                .show();
                        usernameDialog.dismiss();
                    } else {
                        Toast.makeText(AuthActivity.this, R.string.incorrect_field_data, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            usernameCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    App.hideNavigation(getWindow());
                    usernameDialog.dismiss();
                }
            });

            locationOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (country.getText().length() > 0 && city.getText().length() > 0) {
                        myData[2] = String.valueOf(country.getText());
                        myData[3] = String.valueOf(city.getText());
                        createUser(email.getText().toString(), password.getText().toString());
                    } else {
                        Toast.makeText(AuthActivity.this, R.string.incorrect_field_data, Toast.LENGTH_SHORT).show();
                    }
                    App.hideNavigation(getWindow());
                    usernameDialog.dismiss();
                }
            });

            locationCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    App.hideNavigation(getWindow());
                    locationDialog[0].dismiss();
                }
            });
        } else if (email.getText().length() > 0 && password.getText().length() < 6) {
            Toast.makeText(AuthActivity.this, R.string.need_longer_password, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(AuthActivity.this, R.string.please_fill_the_fields, Toast.LENGTH_SHORT).show();
        }
    }

    public void logInClick(View view) {
        if (email.getText().length() > 0 && password.getText().length() > 0) {
            loginUser(email.getText().toString(), password.getText().toString());
        } else {
            Toast.makeText(AuthActivity.this, R.string.incorrect_field_data, Toast.LENGTH_SHORT).show();
        }
    }
}