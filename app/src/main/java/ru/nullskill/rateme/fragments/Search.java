package ru.nullskill.rateme.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.UserProfile;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;

public class Search extends Fragment {

    private Query getUsersListFromName;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getUsersListFromName = App.usersReference.orderByChild("views").limitToLast(50);

        View view = inflater.inflate(R.layout.fragment_search, container, false);
        EditText searchPeopleField = view.findViewById(R.id.search_people_field);
        final LinearLayout usersContainer = view.findViewById(R.id.users_container);
        searchPeopleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                App.hideNavigation(App.window);
            }

            @Override
            public void onTextChanged(CharSequence s, final int start, int before, int count) {
                if (s.length() > 0) {
                    StringBuilder requestBuilder = new StringBuilder();
                    if (String.valueOf(s).contains(" ") && !String.valueOf(s).endsWith(" ")) {
                        String[] words = String.valueOf(s).split(" ");
                        for (String word : words) {
                            requestBuilder.append(word.substring(0, 1).toUpperCase()).append(word.substring(1, word.length())).append(" ");
                            refreshList(inflater, usersContainer);
                        }
                        requestBuilder.deleteCharAt(requestBuilder.length() - 1);
                    } else {
                        requestBuilder.append(String.valueOf(s).substring(0, 1).toUpperCase()).append(String.valueOf(s).substring(1, s.length()));
                        refreshList(inflater, usersContainer);
                    }

                    String request = requestBuilder.toString();
                    getUsersListFromName = App.usersReference.orderByChild("name").startAt(request).endAt(request + "\uf8ff").limitToFirst(30);
                } else {
                    getUsersListFromName = App.usersReference.orderByChild("name").limitToLast(30);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        refreshList(inflater, usersContainer);

        return view;
    }

    private void refreshList(@NonNull final LayoutInflater inflater, final LinearLayout usersContainer) {
        final ArrayList<DataSnapshot> rows = new ArrayList<>();
        getUsersListFromName.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersContainer.removeAllViews();
                rows.clear();
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    rows.add(postSnapshot);
                }

                for (int i = rows.size() - 1; i >= 0; i--) {
                    final DataSnapshot row = rows.get(i);
                    View view = inflater.inflate(R.layout.subscriber_row, null);
                    final CircleImageView searchPhoto = view.findViewById(R.id.search_photo);

                    avatarsStorage.child(row.getKey() + ".jpg")
                            .getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Glide.with(App.context)
                                            .load(uri)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(searchPhoto);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    searchPhoto.setImageResource(R.mipmap.default_user);
                                }
                            });

                    TextView name = view.findViewById(R.id.name);
                    name.setText(String.valueOf(row.child("name").getValue()));
                    TextView location = view.findViewById(R.id.location);
                    TextView views = view.findViewById(R.id.views);
                    location.setText(String.valueOf(row.child("country").getValue() + ", " + row.child("city").getValue()));
                    views.setText(String.valueOf(row.child("views").getValue()));

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(App.context, UserProfile.class)
                                    .putExtra("fragment", 1)
                                    .putExtra("uid", row.getKey()));
                        }
                    });
                    view.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            final ImageView photoDialog = new ImageView(App.context);
                            photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            photoDialog.setImageDrawable(searchPhoto.getDrawable());

                            Glide.with(App.context)
                                    .using(new FirebaseImageLoader())
                                    .load(App.avatarsStorage.child(row.getKey() + ".jpg"))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(photoDialog);

                            new AlertDialog.Builder(App.activity)
                                    .setView(photoDialog)
                                    .show();

                            return true;
                        }
                    });
                    usersContainer.addView(view);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}