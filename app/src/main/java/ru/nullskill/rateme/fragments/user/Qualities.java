package ru.nullskill.rateme.fragments.user;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import ru.nullskill.rateme.App;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.references.Rate;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.notificationReference;
import static ru.nullskill.rateme.App.ratesReference;
import static ru.nullskill.rateme.App.skillsReference;
import static ru.nullskill.rateme.App.usersReference;

public class Qualities extends Fragment {

    private String uid;
    private String myName = "";
    private boolean subscribed = false;
    private ImageView subscribe;
    LayoutInflater inflater;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_qualities, container, false);
        this.inflater = inflater;

        uid = getActivity().getIntent().getStringExtra("uid");
        ImageView back = view.findViewById(R.id.back);
        subscribe = view.findViewById(R.id.subscribe);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        usersReference.child(Storage.getString("UID", "")).child("favorites").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if (postSnapshot.getKey().equals(uid)) {
                        subscribe.setImageResource(R.drawable.subscribed);
                        subscribed = true;
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!subscribed) {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue("");
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue("");
                    subscribe.setImageResource(R.drawable.subscribed);
                } else {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue(null);
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue(null);
                    subscribe.setImageResource(R.drawable.subscribe);
                    subscribed = false;
                }
            }
        });

        final LinearLayout qualitiesContainer = view.findViewById(R.id.qualitiesContainer);
        skillsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                qualitiesContainer.removeAllViews();
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final View qualityLine = inflater.inflate(R.layout.quality_line, null);
                    usersReference.child(uid).child("skills").child(postSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            RoundCornerProgressBar line = qualityLine.findViewById(R.id.line);
                            TextView title = qualityLine.findViewById(R.id.title);
                            TextView points = qualityLine.findViewById(R.id.points);
                            if (Locale.getDefault().getLanguage().equals("ru")) {
                                title.setText(String.valueOf(postSnapshot.child("title_ru").getValue()));
                            } else {
                                title.setText(String.valueOf(postSnapshot.getKey().substring(0, 1).toUpperCase()+postSnapshot.getKey().substring(1,postSnapshot.getKey().length())));
                            }
                            if (dataSnapshot.child("points").getValue() != null) {
                                points.setText(String.valueOf(dataSnapshot.child("points").getValue()));
                                line.setProgress(Float.parseFloat(String.valueOf(dataSnapshot.child("points").getValue())) / 100);
                            } else {
                                points.setText("");
                                line.setProgress(0F);
                            }
                            line.setProgressColor(Color.parseColor(String.valueOf(postSnapshot.child("line_color").getValue())));

                            qualityLine.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View v) {
                                    View dialog = inflater.inflate(R.layout.change_qualities_dialog, null);
                                    final TextView rateButton = dialog.findViewById(R.id.ok);
                                    final TextView unrateButton = dialog.findViewById(R.id.unrate);
                                    TextView dialogTitle = dialog.findViewById(R.id.quality_title);
                                    final EditText comment = dialog.findViewById(R.id.comment);
                                    if (Locale.getDefault().getLanguage().equals("ru")) {
                                        dialogTitle.setText(String.valueOf(postSnapshot.child("title_ru").getValue()));
                                    } else {
                                        dialogTitle.setText(String.valueOf(postSnapshot.getKey().substring(0, 1).toUpperCase()+postSnapshot.getKey().substring(1,postSnapshot.getKey().length())));
                                    }
                                    LinearLayout dialogContainer = dialog.findViewById(R.id.dialog_container);
                                    dialogContainer.setBackgroundColor(Color.parseColor(String.valueOf(postSnapshot.child("line_color").getValue())));

                                    final AlertDialog alertDialog = new AlertDialog.Builder(App.userActivity)
                                            .setView(dialog)
                                            .show();

                                    alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {
                                            App.hideNavigation(App.window);
                                        }
                                    });

                                    usersReference.child(Storage.getString("UID", "")).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            myName = String.valueOf(dataSnapshot.child("name").getValue());
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                    rateButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            usersReference.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    Integer ratesCount;
                                                    Integer points;
                                                    if (dataSnapshot.child("rates").getValue() != null) {
                                                        ratesCount = Integer.valueOf(String.valueOf(dataSnapshot.child("rates").getValue()));
                                                    } else {
                                                        ratesCount = 0;
                                                    }
                                                    if (dataSnapshot.child("skills").child(postSnapshot.getKey()).child("points").getValue() != null) {
                                                        points = Integer.valueOf(String.valueOf(dataSnapshot.child("skills").child(postSnapshot.getKey()).child("points").getValue()));
                                                    } else {
                                                        points = 0;
                                                    }
                                                    ratesCount++;
                                                    points++;
                                                    usersReference.child(uid).child("rates").setValue(ratesCount);
                                                    usersReference.child(uid).child("skills").child(postSnapshot.getKey()).child("points").setValue(points);
                                                    if (comment.getText().length() != 0) {
                                                        ratesReference.child(uid).push().setValue(new Rate(String.valueOf(comment.getText()), 0, postSnapshot.getKey(), 1, Storage.getString("UID", ""), uid));
                                                        notificationReference.child(uid).setValue(myName + " - " + String.valueOf(comment.getText()));
                                                    } else {
                                                        ratesReference.child(uid).push().setValue(new Rate(String.valueOf(comment.getText()), 0, postSnapshot.getKey(), -1, Storage.getString("UID", ""), uid));
                                                        notificationReference.child(uid).setValue(myName + " " + getString(R.string.your_rate));
                                                    }
                                                    if (alertDialog.isShowing()) {
                                                        alertDialog.dismiss();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                            App.hideNavigation(App.window);
                                        }
                                    });
                                    unrateButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            usersReference.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    Integer ratesCount;
                                                    Integer points;
                                                    if (dataSnapshot.child("rates").getValue() != null) {
                                                        ratesCount = Integer.valueOf(String.valueOf(dataSnapshot.child("rates").getValue()));
                                                    } else {
                                                        ratesCount = 0;
                                                    }
                                                    if (dataSnapshot.child("skills").child(postSnapshot.getKey()).child("points").getValue() != null) {
                                                        points = Integer.valueOf(String.valueOf(dataSnapshot.child("skills").child(postSnapshot.getKey()).child("points").getValue()));
                                                    } else {
                                                        points = 0;
                                                    }
                                                    ratesCount++;
                                                    points--;
                                                    usersReference.child(uid).child("rates").setValue(ratesCount);
                                                    usersReference.child(uid).child("skills").child(postSnapshot.getKey()).child("points").setValue(points);
                                                    if (comment.getText().length() != 0) {
                                                        ratesReference.child(uid).push().setValue(new Rate(String.valueOf(comment.getText()), 0, postSnapshot.getKey(), 1, Storage.getString("UID", ""), uid));
                                                        notificationReference.child(uid).setValue(myName + " - " + String.valueOf(comment.getText()));
                                                    } else {
                                                        ratesReference.child(uid).push().setValue(new Rate(String.valueOf(comment.getText()), 0, postSnapshot.getKey(), -1, Storage.getString("UID", ""), uid));
                                                        notificationReference.child(uid).setValue(myName + " " + getString(R.string.your_rate));
                                                    }
                                                    if (alertDialog.isShowing()) {
                                                        alertDialog.dismiss();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                            App.hideNavigation(App.window);
                                        }
                                    });
                                }
                            });
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    qualitiesContainer.addView(qualityLine);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }
}