package ru.nullskill.rateme.fragments;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.BasePermissionListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.model.FeedItem;
import ru.nullskill.rateme.references.User;
import ru.nullskill.rateme.util.Storage;

import static android.app.Activity.RESULT_OK;
import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.postsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class Feed extends Fragment {

    private final int PICK_IMAGE = 1;

    private final ArrayList<FeedItem> posts = new ArrayList<>();
    private LayoutInflater inflater;
    private LinearLayout photoFeed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        photoFeed = view.findViewById(R.id.photoFeed);

        ImageView uploadToFeed = view.findViewById(R.id.uploadToFeed);
        uploadToFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPhoto();
            }
        });

        usersReference.child(Storage.getString("UID", "")).child("feed").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                posts.clear();
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final FeedItem feedItem = new FeedItem();
                    postsStorage.child(Storage.getString("UID", "")).child(postSnapshot.getKey() + ".jpg").getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    feedItem.setPhoto(uri);
                                }
                            })
                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    feedItem.setId(Integer.valueOf(postSnapshot.getKey()));
                                    feedItem.setLikes(postSnapshot.child("likes").getValue());
                                    feedItem.setDislikes(postSnapshot.child("dislikes").getValue());
                                    posts.add(feedItem);
                                    if (posts.size() == dataSnapshot.getChildrenCount()) {
                                        fillFeed();
                                    }
                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

    private void fillFeed() {
        Collections.sort(posts, new FeedComparator());
        photoFeed.removeAllViews();
        for (int i = posts.size() - 1; i >= 0; i--) {
            final int id = posts.get(i).getId();

            View post = inflater.inflate(R.layout.post, null);
            final ImageView imageView = post.findViewById(R.id.postPhoto);
            final TextView likes = post.findViewById(R.id.likes);
            final TextView dislikes = post.findViewById(R.id.dislikes);
            final ImageView likesLine = post.findViewById(R.id.likesLine);
            final ImageView dislikesLine = post.findViewById(R.id.dislikesLine);
            final CircleImageView avatar = post.findViewById(R.id.avatar);
            final TextView author = post.findViewById(R.id.author);

            usersReference.child(Storage.getString("UID", "")).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        author.setText(String.valueOf(user.name));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            avatarsStorage.child(Storage.getString("UID", "") + ".jpg")
                    .getDownloadUrl()
                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(App.context)
                                    .load(uri)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(avatar);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            avatar.setImageResource(R.mipmap.default_user);
                        }
                    });

            Glide.with(App.context)
                    .load(posts.get(i).getPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            usersReference.child(Storage.getString("UID", "")).child("feed").child(String.valueOf(id)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    likes.setText(String.valueOf(dataSnapshot.child("likes").getValue()));
                    dislikes.setText(String.valueOf(dataSnapshot.child("dislikes").getValue()));
                    refreshLine(Integer.parseInt(String.valueOf(likes.getText())), Integer.parseInt(String.valueOf(dislikes.getText())), likesLine, dislikesLine);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usersReference.child(Storage.getString("UID", "")).child("feed").child(String.valueOf(id)).child("likes").setValue(Integer.parseInt(String.valueOf(likes.getText())) + 1);
                }
            });

            dislikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usersReference.child(Storage.getString("UID", "")).child("feed").child(String.valueOf(id)).child("dislikes").setValue(Integer.parseInt(String.valueOf(dislikes.getText())) + 1);
                }
            });

            photoFeed.addView(post);
        }
    }

    private void refreshLine(int likes, int dislikes, ImageView likesLine, ImageView dislikesLine) {
        float likesPercentage = 1f;
        float dislikesPercentage = 1f;
        float percentage = 100f / (likes + dislikes);

        if(likes != 0 && dislikes != 0) {
            likesPercentage = percentage * dislikes;
            dislikesPercentage = percentage * likes;
        }

        LinearLayout.LayoutParams likesParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                16,
                likesPercentage
        );
        LinearLayout.LayoutParams dislikesParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                16,
                dislikesPercentage
        );
        likesLine.setLayoutParams(likesParam);
        dislikesLine.setLayoutParams(dislikesParam);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    final Uri imageUri = imageReturnedIntent.getData();

                    final String newID = String.valueOf((int) System.currentTimeMillis());
                    usersReference.child(Storage.getString("UID", "")).child("feed").child(newID).child("likes").setValue(0);
                    usersReference.child(Storage.getString("UID", "")).child("feed").child(newID).child("dislikes").setValue(0);

                    StorageReference uploadReference = postsStorage.child(Storage.getString("UID", "")).child(newID + ".jpg");
                    UploadTask uploadTask = uploadReference.putFile(imageUri);

                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            usersReference.child(Storage.getString("UID", "")).child("feed").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                                    posts.clear();
                                    for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                        final FeedItem feedItem = new FeedItem();
                                        postsStorage.child(Storage.getString("UID", "")).child(postSnapshot.getKey() + ".jpg").getDownloadUrl()
                                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                    @Override
                                                    public void onSuccess(Uri uri) {
                                                        feedItem.setPhoto(uri);
                                                    }
                                                })
                                                .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Uri> task) {
                                                        feedItem.setId(Integer.valueOf(postSnapshot.getKey()));
                                                        feedItem.setLikes(postSnapshot.child("likes").getValue());
                                                        feedItem.setDislikes(postSnapshot.child("dislikes").getValue());
                                                        posts.add(feedItem);
                                                        if (posts.size() == dataSnapshot.getChildrenCount()) {
                                                            fillFeed();
                                                        }
                                                    }
                                                });
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    });
                }
        }
    }

    private void uploadPhoto() {
        Dexter
                .withActivity(App.activity)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new BasePermissionListener() {

                })
                .check();
        Dexter
                .withActivity(App.activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new BasePermissionListener() {

                })
                .check();

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE);
    }

    class FeedComparator implements Comparator<FeedItem> {
        public int compare(FeedItem feedItem1, FeedItem feedItem2) {
            if (feedItem1.getId() > feedItem2.getId()) {
                return 1;
            } else if (feedItem1.getId() < feedItem2.getId()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}