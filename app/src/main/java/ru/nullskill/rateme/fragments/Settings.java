package ru.nullskill.rateme.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.BasePermissionListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.AuthActivity;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.util.Storage;

import static android.app.Activity.RESULT_OK;
import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.thumbsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class Settings extends Fragment {

    private final int PICK_IMAGE = 1;

    private CircleImageView settingsAvatar;
    private LayoutInflater inflater;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        settingsAvatar = view.findViewById(R.id.settings_profile_photo);
        TextView changeText = view.findViewById(R.id.settings_profile_text);

        settingsAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePhoto();
            }
        });

        changeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePhoto();
            }
        });

        Switch notificationSwitch = view.findViewById(R.id.notifications);
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Storage.addBoolean("enable_notification", true);
                } else {
                    Storage.addBoolean("enable_notification", false);
                }
            }
        });

        thumbsStorage.child(Storage.getString("UID", "") + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(App.context)
                        .load(uri)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(settingsAvatar);
            }
        });

        TextView myUserName = view.findViewById(R.id.my_user_name);
        myUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View myUserNameDialog = inflater.inflate(R.layout.my_username_dialog, null);
                final EditText name = myUserNameDialog.findViewById(R.id.name);
                final EditText surname = myUserNameDialog.findViewById(R.id.surname);
                TextView ok = myUserNameDialog.findViewById(R.id.ok);
                TextView cancel = myUserNameDialog.findViewById(R.id.cancel);

                final AlertDialog dialog = new AlertDialog.Builder(App.activity)
                        .setView(myUserNameDialog)
                        .setCancelable(false)
                        .show();

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        usersReference.child(Storage.getString("UID", "")).child("name").setValue(String.valueOf(name.getText() + " " + surname.getText()));
                        App.hideNavigation(App.window);
                        dialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        App.hideNavigation(App.window);
                        dialog.dismiss();
                    }
                });
            }
        });

        TextView myLocation = view.findViewById(R.id.my_location);
        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View myLocationDialog = inflater.inflate(R.layout.my_location_dialog, null);
                final EditText country = myLocationDialog.findViewById(R.id.country);
                final EditText city = myLocationDialog.findViewById(R.id.city);
                TextView ok = myLocationDialog.findViewById(R.id.ok);
                TextView cancel = myLocationDialog.findViewById(R.id.cancel);

                final AlertDialog dialog = new AlertDialog.Builder(App.activity)
                        .setView(myLocationDialog)
                        .setCancelable(false)
                        .show();

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        usersReference.child(Storage.getString("UID", "")).child("country").setValue(String.valueOf(country.getText()));
                        usersReference.child(Storage.getString("UID", "")).child("city").setValue(String.valueOf(city.getText()));
                        App.hideNavigation(App.window);
                        dialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        App.hideNavigation(App.window);
                        dialog.dismiss();
                    }
                });
            }
        });

        TextView changeUser = view.findViewById(R.id.change_user);
        changeUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Storage.clear("uid");
                startActivity(new Intent(App.context, AuthActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    final ProgressDialog uploadPhoto = new ProgressDialog(App.activity);
                    uploadPhoto.setCancelable(false);
                    uploadPhoto.setMessage(getString(R.string.upload_photo));
                    uploadPhoto.show();

                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = App.activity.getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                        Bitmap croppedLarge;
                        Bitmap croppedThumb;
                        if (selectedImage.getWidth() > selectedImage.getHeight()) {
                            float width = selectedImage.getWidth();
                            float height = selectedImage.getHeight();
                            float newHeight = height * (640 / width);
                            croppedLarge = Bitmap.createScaledBitmap(selectedImage, 640, (int) newHeight, false);
                        } else if (selectedImage.getWidth() < selectedImage.getHeight()) {
                            float width = selectedImage.getWidth();
                            float height = selectedImage.getHeight();
                            float newWidth = width * (640 / height);
                            croppedLarge = Bitmap.createScaledBitmap(selectedImage, (int) newWidth, 640, false);
                        } else {
                            croppedLarge = Bitmap.createScaledBitmap(selectedImage, 640, 640, false);
                        }

                        if (selectedImage.getWidth() > selectedImage.getHeight()) {
                            float width = selectedImage.getWidth();
                            float height = selectedImage.getHeight();
                            float newHeight = height * (180 / width);
                            croppedThumb = Bitmap.createScaledBitmap(selectedImage, 180, (int) newHeight, false);
                        } else if (selectedImage.getWidth() < selectedImage.getHeight()) {
                            float width = selectedImage.getWidth();
                            float height = selectedImage.getHeight();
                            float newWidth = width * (180 / height);
                            croppedThumb = Bitmap.createScaledBitmap(selectedImage, (int) newWidth, 180, false);
                        } else {
                            croppedThumb = Bitmap.createScaledBitmap(selectedImage, 180, 180, false);
                        }

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ByteArrayOutputStream thumbBaos = new ByteArrayOutputStream();
                        croppedLarge.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        croppedThumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbBaos);
                        byte[] data = baos.toByteArray();
                        byte[] thumbData = thumbBaos.toByteArray();

                        View profileView = inflater.inflate(R.layout.fragment_profile, null);
                        final ImageView avatar = profileView.findViewById(R.id.main_photo);

                        StorageReference uploadReference = avatarsStorage.child(Storage.getString("UID", "") + ".jpg");
                        UploadTask uploadTask = uploadReference.putBytes(data);
                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                avatarsStorage.child(Storage.getString("UID", "") + ".jpg")
                                        .getDownloadUrl()
                                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                Glide.with(App.context)
                                                        .load(uri)
                                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                        .into(avatar);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                avatar.setImageResource(R.mipmap.default_user);
                                            }
                                        });
                            }
                        });

                        StorageReference thumbUploadReference = thumbsStorage.child(Storage.getString("UID", "") + ".jpg");
                        UploadTask thumbUploadTask = thumbUploadReference.putBytes(thumbData);
                        thumbUploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                thumbsStorage.child(Storage.getString("UID", "") + ".jpg")
                                        .getDownloadUrl()
                                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                Glide.with(App.context)
                                                        .load(uri)
                                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                        .into(settingsAvatar);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                settingsAvatar.setImageResource(R.mipmap.default_user);
                                            }
                                        });
                            }
                        });

                        if (uploadPhoto.isShowing()) {
                            uploadPhoto.dismiss();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    private void changePhoto() {
        Dexter
                .withActivity(App.activity)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new BasePermissionListener() {

                })
                .check();
        Dexter
                .withActivity(App.activity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new BasePermissionListener() {

                })
                .check();

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, PICK_IMAGE);
    }
}