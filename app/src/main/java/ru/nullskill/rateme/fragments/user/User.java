package ru.nullskill.rateme.fragments.user;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.model.FeedItem;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.postsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class User extends Fragment {

    private CircleImageView mainPhoto;
    private TextView userName, location, rates, views;
    private String uid;
    private Uri avatarUri;
    private final ArrayList<FeedItem> posts = new ArrayList<>();
    private LayoutInflater inflater;
    private LinearLayout photoFeed;
    private boolean subscribed = false;
    private ImageView subscribe;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_user, container, false);
        this.inflater = inflater;

        uid = getActivity().getIntent().getStringExtra("uid");

        mainPhoto = view.findViewById(R.id.main_photo);
        userName = view.findViewById(R.id.user_name);
        location = view.findViewById(R.id.location);
        rates = view.findViewById(R.id.rates);
        views = view.findViewById(R.id.views);
        photoFeed = view.findViewById(R.id.photoFeed);
        ImageView back = view.findViewById(R.id.back);
        subscribe = view.findViewById(R.id.subscribe);

        mainPhoto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ImageView photoDialog = new ImageView(App.context);
                photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                photoDialog.setImageDrawable(mainPhoto.getDrawable());
                Glide.with(App.context).load(avatarUri)
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(photoDialog);
                new AlertDialog.Builder(App.userActivity)
                        .setView(photoDialog)
                        .show()
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                App.hideNavigation(App.window);
                            }
                        });
                return true;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        usersReference.child(Storage.getString("UID", "")).child("favorites").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if (postSnapshot.getKey().equals(uid)) {
                        subscribe.setImageResource(R.drawable.subscribed);
                        subscribed = true;
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!subscribed) {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue("");
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue("");
                    subscribe.setImageResource(R.drawable.subscribed);
                } else {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue(null);
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue(null);
                    subscribe.setImageResource(R.drawable.subscribe);
                    subscribed = false;
                }
            }
        });

        usersReference.child(uid).child("views").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Integer viewsCount = Integer.valueOf(String.valueOf(dataSnapshot.getValue()));
                viewsCount++;
                usersReference.child(uid).child("views").setValue(viewsCount);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        usersReference.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userName.setText(String.valueOf(dataSnapshot.child("name").getValue()));
                location.setText(String.valueOf(dataSnapshot.child("country").getValue() + ", " + dataSnapshot.child("city").getValue()));
                rates.setText(String.valueOf(dataSnapshot.child("rates").getValue()));
                views.setText(String.valueOf(dataSnapshot.child("views").getValue()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final int[] rankPoints = {0};
        usersReference.child(uid).child("skills").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                rankPoints[0] = 0;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    rankPoints[0] += Integer.valueOf(String.valueOf(postSnapshot.child("points").getValue()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        avatarsStorage.child(uid + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                avatarUri = uri;
                Glide.with(App.context)
                        .load(avatarUri)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mainPhoto);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        });

        usersReference.child(uid).child("feed").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                posts.clear();
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final FeedItem feedItem = new FeedItem();
                    postsStorage.child(uid).child(postSnapshot.getKey() + ".jpg").getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    feedItem.setPhoto(uri);
                                }
                            })
                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    feedItem.setId(Integer.valueOf(postSnapshot.getKey()));
                                    feedItem.setLikes(postSnapshot.child("likes").getValue());
                                    feedItem.setDislikes(postSnapshot.child("dislikes").getValue());
                                    posts.add(feedItem);
                                    if (posts.size() == dataSnapshot.getChildrenCount()) {
                                        fillFeed();
                                    }
                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

    private void fillFeed() {
        Collections.sort(posts, new FeedComparator());
        photoFeed.removeAllViews();
        for (int i = posts.size() - 1; i >= 0; i--) {
            final int id = posts.get(i).getId();

            View post = inflater.inflate(R.layout.post, null);
            final ImageView imageView = post.findViewById(R.id.postPhoto);
            final TextView likes = post.findViewById(R.id.likes);
            final TextView dislikes = post.findViewById(R.id.dislikes);
            final ImageView likesLine = post.findViewById(R.id.likesLine);
            final ImageView dislikesLine = post.findViewById(R.id.dislikesLine);
            final CircleImageView avatar = post.findViewById(R.id.avatar);
            final TextView author = post.findViewById(R.id.author);

            usersReference.child(uid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    ru.nullskill.rateme.references.User user = dataSnapshot.getValue(ru.nullskill.rateme.references.User.class);
                    if (user != null) {
                        author.setText(String.valueOf(user.name));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            avatarsStorage.child(uid + ".jpg")
                    .getDownloadUrl()
                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(App.context)
                                    .load(uri)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(avatar);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            avatar.setImageResource(R.mipmap.default_user);
                        }
                    });

            Glide.with(App.context)
                    .load(posts.get(i).getPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            usersReference.child(uid).child("feed").child(String.valueOf(id)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    likes.setText(String.valueOf(dataSnapshot.child("likes").getValue()));
                    dislikes.setText(String.valueOf(dataSnapshot.child("dislikes").getValue()));
                    refreshLine(Integer.parseInt(String.valueOf(likes.getText())), Integer.parseInt(String.valueOf(dislikes.getText())), likesLine, dislikesLine);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usersReference.child(uid).child("feed").child(String.valueOf(id)).child("likes").setValue(Integer.parseInt(String.valueOf(likes.getText())) + 1);
                }
            });

            dislikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usersReference.child(uid).child("feed").child(String.valueOf(id)).child("dislikes").setValue(Integer.parseInt(String.valueOf(dislikes.getText())) + 1);
                }
            });

            photoFeed.addView(post);
        }
    }

    private void refreshLine(int likes, int dislikes, ImageView likesLine, ImageView dislikesLine) {
        float likesPercentage = 1f;
        float dislikesPercentage = 1f;
        float percentage = 100f / (likes + dislikes);

        if (likes != 0 && dislikes != 0) {
            likesPercentage = percentage * dislikes;
            dislikesPercentage = percentage * likes;
        }

        LinearLayout.LayoutParams likesParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                16,
                likesPercentage
        );
        LinearLayout.LayoutParams dislikesParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                16,
                dislikesPercentage
        );
        likesLine.setLayoutParams(likesParam);
        dislikesLine.setLayoutParams(dislikesParam);
    }

    class FeedComparator implements Comparator<FeedItem> {
        public int compare(FeedItem feedItem1, FeedItem feedItem2) {
            if (feedItem1.getId() > feedItem2.getId()) {
                return 1;
            } else if (feedItem1.getId() < feedItem2.getId()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
