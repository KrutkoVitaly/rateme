package ru.nullskill.rateme.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.Dialogs;
import ru.nullskill.rateme.History;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.Subscribers;
import ru.nullskill.rateme.UserProfile;
import ru.nullskill.rateme.references.Rate;
import ru.nullskill.rateme.references.User;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.ratesReference;
import static ru.nullskill.rateme.App.skillsReference;
import static ru.nullskill.rateme.App.thumbsStorage;
import static ru.nullskill.rateme.App.usersReference;

public class Profile extends Fragment {

    private TextView userName;
    private TextView location;
    private TextView rates;
    private TextView views;
    private TextView rank;
    private LinearLayout profileContainer;
    private TextView subscribersCount;
    private CircleImageView avatar;
    private int mails = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        avatar = view.findViewById(R.id.main_photo);
        profileContainer = view.findViewById(R.id.profile_container);
        rank = view.findViewById(R.id.rank);
        userName = view.findViewById(R.id.user_name);
        location = view.findViewById(R.id.location);
        rates = view.findViewById(R.id.rates);
        views = view.findViewById(R.id.views);
        LinearLayout dialogs = view.findViewById(R.id.dialogs);
        LinearLayout subscribers = view.findViewById(R.id.subscribers);
        final TextView mailCount = view.findViewById(R.id.mailCount);
        subscribersCount = view.findViewById(R.id.subscribersCount);

        subscribers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(App.context, Subscribers.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        dialogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(App.context, Dialogs.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        mailCount.setText(String.valueOf(mails));
        usersReference.child(Storage.getString("UID", "")).child("dialogs").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot dialogs : dataSnapshot.getChildren()) {
                    usersReference.child(Storage.getString("UID", "")).child("dialogs").child(dialogs.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (final DataSnapshot uids : dataSnapshot.getChildren()) {
                                usersReference.child(Storage.getString("UID", "")).child("dialogs").child(dialogs.getKey()).child(uids.getKey()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        usersReference.child(Storage.getString("UID", "")).child("dialogs").child(dialogs.getKey()).child(uids.getKey()).addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (!String.valueOf(dataSnapshot.child("sender").getValue()).equals(Storage.getString("UID", ""))) {
                                                    if (String.valueOf(dataSnapshot.child("receiverStatus").getValue()).equals("new")) {
                                                        mails++;
                                                        mailCount.setText(String.valueOf(mails));
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        usersReference.child(Storage.getString("UID", "")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    userName.setText(String.valueOf(user.name));
                    location.setText(String.valueOf(user.country + ", " + user.city));
                    rates.setText(String.valueOf(user.rates));
                    views.setText(String.valueOf(user.views));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        usersReference.child(Storage.getString("UID", "")).child("subscribers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subscribersCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        usersReference.child(Storage.getString("UID", "")).child("skills").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int rankPoints = 0;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    rankPoints += Integer.valueOf(String.valueOf(postSnapshot.child("points").getValue()));
                }
                rank.setText(String.valueOf(rankPoints));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        avatarsStorage.child(Storage.getString("UID", "") + ".jpg")
                .getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Glide.with(App.context)
                                .load(uri)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(avatar);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        avatar.setImageResource(R.mipmap.default_user);
                    }
                });

        avatar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                avatarsStorage.child(Storage.getString("UID", "") + ".jpg")
                        .getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                ImageView photoDialog = new ImageView(App.context);
                                photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                                Glide.with(App.context)
                                        .load(uri)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(photoDialog);

                                new AlertDialog.Builder(App.activity)
                                        .setView(photoDialog)
                                        .show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
                return true;
            }
        });

        final View adContainer = inflater.inflate(R.layout.ad_card, null);
        LinearLayout adContainerLayout = adContainer.findViewById(R.id.ad_container);

        final View toHistoryContainer = inflater.inflate(R.layout.ad_card, null);
        TextView toHistoryButton = toHistoryContainer.findViewById(R.id.show_history_button);
        toHistoryButton.setVisibility(View.VISIBLE);
        toHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), History.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        final AdView mAdView = new AdView(App.activity);
        mAdView.setAdUnitId(getString(R.string.adUnitId));
        mAdView.setAdSize(AdSize.FLUID);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        adContainerLayout.addView(mAdView);

        Query getMyLastRates = ratesReference.child(Storage.getString("UID", "")).limitToLast(5);
        getMyLastRates.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                profileContainer.removeAllViews();
                profileContainer.addView(adContainer);
                ArrayList<View> viewSet = new ArrayList<>();
                viewSet.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final Rate rate = postSnapshot.getValue(Rate.class);
                    if (rate != null) {
                        View rateCard = inflater.inflate(R.layout.last_rate_card, null);
                        final CircleImageView rateAvatar = rateCard.findViewById(R.id.rate_avatar);
                        final TextView name = rateCard.findViewById(R.id.name);
                        final TextView comment = rateCard.findViewById(R.id.comment);
                        final TextView quality = rateCard.findViewById(R.id.quality);

                        if (rate.quality != null) {
                            Query getSkillsRates = skillsReference.child(rate.quality);
                            getSkillsRates.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String text = "";
                                    if (rate.points == -1) {
                                        quality.setBackgroundResource(R.drawable.black_round_corners);
                                        if (Locale.getDefault().getLanguage().equals("ru")) {
                                            text = String.valueOf("- " + dataSnapshot.child("title_ru").getValue());
                                        } else {
                                            text = String.valueOf("- " + dataSnapshot.getKey().substring(0, 1).toUpperCase()+dataSnapshot.getKey().substring(1,dataSnapshot.getKey().length()));
                                        }
                                    }
                                    if (rate.points == 1) {
                                        quality.setBackgroundResource(R.drawable.round_corners);
                                        StateListDrawable shapeDrawable = (StateListDrawable) quality.getBackground();
                                        shapeDrawable.setColorFilter(Color.parseColor(String.valueOf(dataSnapshot.child("line_color").getValue())), PorterDuff.Mode.MULTIPLY);
                                        if (Locale.getDefault().getLanguage().equals("ru")) {
                                            text = String.valueOf("+ " + dataSnapshot.child("title_ru").getValue());
                                        } else {
                                            text = String.valueOf("+ " + dataSnapshot.getKey().substring(0, 1).toUpperCase()+dataSnapshot.getKey().substring(1,dataSnapshot.getKey().length()));
                                        }
                                    }
                                    quality.setText(text);
                                    quality.setTextColor(Color.WHITE);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }

                        if (String.valueOf(rate.comment).length() == 0) {
                            comment.setVisibility(View.GONE);
                        } else {
                            comment.setText(String.valueOf(rate.comment));
                        }
                        rateAvatar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(App.context, UserProfile.class)
                                        .putExtra("fragment", 1)
                                        .putExtra("uid", rate.sender));
                            }
                        });
                        rateAvatar.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                final ImageView photoDialog = new ImageView(App.context);
                                photoDialog.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                photoDialog.setImageDrawable(rateAvatar.getDrawable());

                                avatarsStorage.child(rate.sender + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Glide.with(App.context)
                                                .load(uri)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .into(photoDialog);

                                        new AlertDialog.Builder(App.activity)
                                                .setView(photoDialog)
                                                .show();

                                    }
                                });

                                return true;
                            }
                        });
                        usersReference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                name.setText(String.valueOf(dataSnapshot.child(rate.sender).child("name").getValue()));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        thumbsStorage.child(rate.sender + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Glide.with(App.context)
                                        .load(uri)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(rateAvatar);
                            }
                        });
                        viewSet.add(rateCard);
                    }
                }
                for (int i = viewSet.size() - 1; i >= 0; i--) {
                    profileContainer.addView(viewSet.get(i));
                }
                profileContainer.addView(toHistoryContainer);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }
}