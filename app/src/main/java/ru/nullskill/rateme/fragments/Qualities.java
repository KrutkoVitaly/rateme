package ru.nullskill.rateme.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import ru.nullskill.rateme.R;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.skillsReference;
import static ru.nullskill.rateme.App.usersReference;

public class Qualities extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qualities, container, false);

        final LinearLayout qualitiesContainer = view.findViewById(R.id.quality_container);

        skillsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                qualitiesContainer.removeAllViews();
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    final View qualityLine = inflater.inflate(R.layout.quality_line, null);
                    usersReference.child(Storage.getString("UID", "")).child("skills").child(postSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            RoundCornerProgressBar line = qualityLine.findViewById(R.id.line);
                            TextView title = qualityLine.findViewById(R.id.title);
                            TextView points = qualityLine.findViewById(R.id.points);
                            if (Locale.getDefault().getLanguage().equals("ru")) {
                                title.setText(String.valueOf(postSnapshot.child("title_ru").getValue()));
                            } else {
                                title.setText(String.valueOf(postSnapshot.getKey().substring(0, 1).toUpperCase()+postSnapshot.getKey().substring(1,postSnapshot.getKey().length())));
                            }
                            if (dataSnapshot.child("points").getValue() != null) {
                                points.setText(String.valueOf(dataSnapshot.child("points").getValue()));
                                line.setProgress(Float.parseFloat(String.valueOf(dataSnapshot.child("points").getValue())) / 100);
                            } else {
                                points.setText("");
                                line.setProgress(0F);
                            }
                            line.setProgressColor(Color.parseColor(String.valueOf(postSnapshot.child("line_color").getValue())));
                            points.setText(String.valueOf(dataSnapshot.child("points").getValue() != null ? dataSnapshot.child("points").getValue() : ""));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    qualitiesContainer.addView(qualityLine);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }
}