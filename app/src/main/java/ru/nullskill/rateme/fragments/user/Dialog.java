package ru.nullskill.rateme.fragments.user;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nullskill.rateme.App;
import ru.nullskill.rateme.R;
import ru.nullskill.rateme.references.Message;
import ru.nullskill.rateme.util.Storage;

import static ru.nullskill.rateme.App.avatarsStorage;
import static ru.nullskill.rateme.App.userActivity;
import static ru.nullskill.rateme.App.usersReference;

public class Dialog extends Fragment {

    private String uid;
    private boolean subscribed = false;
    private ImageView subscribe;
    private LayoutInflater inflater;
    private LinearLayout dialogContainer;
    private ScrollView dialogScroller;
    private boolean isScroll = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_dialog, container, false);
        this.inflater = inflater;

        uid = getActivity().getIntent().getStringExtra("uid");
        ImageView back = view.findViewById(R.id.back);
        ImageView send = view.findViewById(R.id.send);
        subscribe = view.findViewById(R.id.subscribe);
        final EditText input = view.findViewById(R.id.message);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        usersReference.child(Storage.getString("UID", "")).child("favorites").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if (postSnapshot.getKey().equals(uid)) {
                        subscribe.setImageResource(R.drawable.subscribed);
                        subscribed = true;
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!subscribed) {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue("");
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue("");
                    subscribe.setImageResource(R.drawable.subscribed);
                } else {
                    usersReference.child(uid).child("subscribers").child(Storage.getString("UID", "")).setValue(null);
                    usersReference.child(Storage.getString("UID", "")).child("favorites").child(uid).setValue(null);
                    subscribe.setImageResource(R.drawable.subscribe);
                    subscribed = false;
                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersReference.child(uid).child("dialogs").child(Storage.getString("UID", ""))
                        .push()
                        .setValue(new Message(
                                String.valueOf(input.getText()),
                                Storage.getString("UID", ""),
                                "read",
                                "new"
                        )).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        input.setText("");
                        dialogScroller.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                dialogScroller.post(new Runnable() {
                                    public void run() {
                                        dialogScroller.fullScroll(View.FOCUS_DOWN);
                                    }
                                });
                            }
                        });
                    }
                });
                if (!uid.equals(Storage.getString("UID", ""))) {
                    usersReference.child(Storage.getString("UID", "")).child("dialogs").child(uid)
                            .push()
                            .setValue(new Message(
                                    String.valueOf(input.getText()),
                                    Storage.getString("UID", ""),
                                    "new",
                                    "read"
                            )).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            input.setText("");
                            dialogScroller.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    dialogScroller.post(new Runnable() {
                                        public void run() {
                                            dialogScroller.fullScroll(View.FOCUS_DOWN);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });

        dialogContainer = view.findViewById(R.id.dialog_container);
        dialogScroller = view.findViewById(R.id.dialog_scroller);
        dialogScroller.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (dialogScroller != null) {
                    if (dialogScroller.getChildAt(0).getBottom() <= (dialogScroller.getHeight() + dialogScroller.getScrollY())) {
                        isScroll = true;
                    } else {
                        isScroll = false;
                    }
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialogScroller.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                }
            });
        }

        usersReference.child(Storage.getString("UID", "")).child("dialogs").child(uid).limitToLast(100).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dialogContainer.removeAllViews();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    View message = null;
                    if (postSnapshot.child("sender").getValue().equals(uid)) {
                        message = inflater.inflate(R.layout.receiver_message, null);
                    } else {
                        message = inflater.inflate(R.layout.sender_message, null);
                        if (String.valueOf(postSnapshot.child("receiverStatus").getValue()).equals("new")) {
                            usersReference.child(Storage.getString("UID", "")).child("dialogs").child(uid).child(postSnapshot.getKey()).child("receiverStatus").setValue("read");
                            if (!uid.equals(Storage.getString("UID", ""))) {
                                usersReference.child(uid).child("dialogs").child(Storage.getString("UID", "")).child(postSnapshot.getKey()).child("senderStatus").setValue("read");
                            }
                            playSound();
                        }
                    }
                    TextView text = message.findViewById(R.id.message_text);
                    TextView time = message.findViewById(R.id.message_time);
                    final CircleImageView avatar = message.findViewById(R.id.avatar);

                    text.setText(String.valueOf(postSnapshot.child("messageText").getValue()));

                    time.setText(DateFormat.format("dd.MM.yyyy HH:mm", Long.parseLong(String.valueOf(postSnapshot.child("messageTime").getValue()))));
                    avatarsStorage.child(String.valueOf(postSnapshot.child("sender").getValue()) + ".jpg").getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Glide
                                            .with(App.context)
                                            .load(uri)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(avatar);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    avatar.setImageResource(R.mipmap.default_user);
                                }
                            })
                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (!isScroll) {
                                        dialogScroller.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                            @Override
                                            public void onGlobalLayout() {
                                                dialogScroller.post(new Runnable() {
                                                    public void run() {
                                                        dialogScroller.fullScroll(View.FOCUS_DOWN);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                    dialogContainer.addView(message);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

    private void playSound() {
        if (!userActivity.isFinishing()) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(App.context)
                    .setSmallIcon(R.mipmap.notification)
                    .setLargeIcon(bitmap)
                    .setContentTitle("RateMe")
                    .setContentText(String.valueOf("New messages"))
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[]{0, 300, 0, 300, 0, 300});

            NotificationManager notificationManager = (NotificationManager) App.userActivity.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notificationBuilder.build());
            }
        }
    }
}